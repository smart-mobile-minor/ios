//
//  Case.swift
//  IOShelloworld
//
//  Created by Sid Michiels on 15/10/2021.
//

import Foundation

struct Case : PCPart {
    var id: Int
    var name : String
    var brand : String
    var size : String
    var image : String
}
