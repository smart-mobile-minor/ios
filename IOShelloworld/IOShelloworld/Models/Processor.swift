//
//  Cpu.swift
//  IOShelloworld
//
//  Created by Sid Michiels on 15/10/2021.
//

import Foundation

struct Processor : PCPart {
    var id: Int
    var brand: String
    var name : String
    var socketType : String
    var image : String
}
