//
//  Inject.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation

@propertyWrapper
struct Inject<T> {
    private let key : String
    var wrappedValue : T {
        get {
            Injectables[key]
        }
    }
    
    init(_ key : String){
        self.key = key
    }
}
