//
//  AssemblyStep.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import Foundation

struct AssemblyStep : Encodable, Decodable, Identifiable {
    var id: Int
    var name : String
    var description : String
    var armodels: [String]
    var sources: String
    var video: String
    var completed : Bool
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        description = try values.decode(String.self, forKey: .description)
        armodels = try values.decode([String].self, forKey: .armodels)
        sources = try values.decode(String.self, forKey: .sources)
        video = try values.decode(String.self, forKey: .video)
        //var strings - values.decode(<#T##Bool#>, forKey: <#T##KeyedDecodingContainer<K>#>)
        //armodels = [ARModel(model: string)]
        
        completed = false
        
    }
    
}
