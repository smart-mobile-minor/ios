//
//  IOShelloworldApp.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 28/09/2021.
//

import SwiftUI

@main
struct IOShelloworldApp: App {
    init() {
        APIServices.addApiServicesToInjectables()
        ViewModels.addViewModelsToInjectables()
        Views.addViewsToInjectables()
    }
    
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
