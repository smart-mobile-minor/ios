//
//  ARModel.swift
//  IOShelloworld
//
//  Created by Sid Michiels on 10/11/2021.
//

import UIKit
import RealityKit
import Combine

import Alamofire

class ARModel {
    var modelName: String
    var image: UIImage?
    var modelEntity: ModelEntity?
//    var assemblyStepId: Int
    
    private var cancellable: AnyCancellable?
    
    init(modelName: String, completion: @escaping (ARModel) -> Void){
        self.modelName = modelName
        self.image = UIImage(named: modelName)
        
        let documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let fileURL = documentsURL.appendingPathComponent(modelName + ".usdz")
        let downloadurl = "http://fe9e-145-93-172-69.ngrok.io/files/models/" + modelName + ".usdz"
        let destination : DownloadRequest.Destination = {_, _ in
            return (fileURL, [.removePreviousFile, .createIntermediateDirectories])
        }
        
        AF.download(
            downloadurl,
            to: destination
        ).response{ response in
            debugPrint(response)
            
            self.cancellable = ModelEntity.loadModelAsync(contentsOf: fileURL, withName: modelName)
                .sink(receiveCompletion: { loadCompletion in
                    print("DEBUG: Unable to load modelEntity for modelName: \(self.modelName)")
                }, receiveValue: { modelEntity in
                    self.modelEntity = modelEntity
                    completion(self)
                    print("DEBUG: Succesfully to load modelEntity for modelName: \(self.modelName)")
                })
        }
    }
}
