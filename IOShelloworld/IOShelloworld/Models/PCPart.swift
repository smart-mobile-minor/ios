//
//  PCPart.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 04/11/2021.
//

import Foundation

protocol PCPart : Decodable, Encodable, Identifiable, Codable {
    var id: Int {get}
    var brand: String {get}
    var name: String {get}
}
