//
//  AssemblyOverviewView.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import SwiftUI

struct AssemblyOverviewView: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var assemblyOverviewViewModel : AssemblyOverviewViewModel
    
    private var completeColor : Color = .green
    private var disabledColor : Color = .gray
    private var normalColor : Color {
        return colorScheme == .dark ? .white : .black
    }
    
    init(assemblyOverviewViewModel : AssemblyOverviewViewModel){
        self.assemblyOverviewViewModel = assemblyOverviewViewModel
    }
    
    var body: some View {
        VStack(spacing: 20){
            List(Array(assemblyOverviewViewModel.assemblySteps.enumerated()), id: \.offset){ index, assemblyStep in
                 
                    HStack {
                        Text("\(index + 1)").foregroundColor(index + 1 > assemblyOverviewViewModel.currentStepNumber ? disabledColor : normalColor)
                        Text(assemblyStep.name).foregroundColor(index + 1 > assemblyOverviewViewModel.currentStepNumber ? disabledColor : normalColor)
                        Spacer()
                        Image(systemName: assemblyStep.completed ? "checkmark" : "chevron.right")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 7)
                            .foregroundColor({
                                () -> Color in
                                if (assemblyStep.completed) {return completeColor}
                                if (index + 1 == assemblyOverviewViewModel.currentStepNumber) {return normalColor}
                                return disabledColor
                            }())
                    }
                    .background(
                        NavigationLink(destination: AssemblyStepView(step : assemblyStep, assemblyOverviewViewModel: assemblyOverviewViewModel)) {}
                            .opacity(0)
                            .disabled(
                                index + 1 > assemblyOverviewViewModel.currentStepNumber
                            )
                    )
                
            }
                NavigationLink(destination: ContentView()) {
                    Text("Done")
                }
                .navigationTitle("Steps \(assemblyOverviewViewModel.currentStepNumber)/\(assemblyOverviewViewModel.assemblyStepsLength)")
        }.onAppear{assemblyOverviewViewModel.getAssemblySteps()}
    }
}

