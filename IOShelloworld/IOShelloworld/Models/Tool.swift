//
//  Tool.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import Foundation

struct Tool : Codable, Identifiable {
    var id: Int
    var name : String
    var description : String
    var image : String
    var video: String
}
