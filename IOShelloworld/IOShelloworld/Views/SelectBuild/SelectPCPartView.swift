//
//  SelectPCPartView.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 04/11/2021.
//

import SwiftUI

struct SelectPCPartView<T : PCPart> : View {
    @ObservedObject var selectPartViewModel : SelectPCPartViewModel<T>
    @ObservedObject var buildViewModel : PCBuildViewModel
    private var nextPage : AnyView
    private var partName : String
    
    init(selectPartViewModel : SelectPCPartViewModel<T>,buildViewModel : PCBuildViewModel, nextPage: AnyView, partName: String) {
        self.selectPartViewModel = selectPartViewModel
        self.buildViewModel = buildViewModel
        self.nextPage = nextPage
        self.partName = partName
    }
    
    var body: some View {
        VStack(spacing: 10){
            Text("Pick a \(partName)")
            Form{
                
                Picker(selection: $selectPartViewModel.selectedPartBrand, label: Text("Select a \(partName) brand")){
                    ForEach(selectPartViewModel.partBrands, id: \.self) { partbrand in
                            Text(partbrand)
                        }
                }
                
                Picker(selection: $selectPartViewModel.selectedPartId, label: Text("Select a \(partName)")){
                        ForEach(selectPartViewModel.usableParts) { part in
                            Text(part.name).tag(part.id)
                        }
                }.disabled(selectPartViewModel.selectedPartBrand.isEmpty)
                
                Image("cpu")
                    .resizable()
                    .frame(height: 80)
                    .aspectRatio(1/1, contentMode: .fit)
                    .background(Color.white)
                    .cornerRadius(12)
            }
            
            HStack(spacing: 20) {
                NavigationLink(destination: nextPage) {
                    Text("Next")
                }.disabled(selectPartViewModel.selectedPart == nil)
                .navigationTitle("Select \(partName)")
            }
        }
        .onAppear{selectPartViewModel.getParts()}
        .onDisappear{
            guard let part = selectPartViewModel.selectedPart else {
                return
            }
            buildViewModel.setPart(
                part: part as AnyObject
                )
            }
    }
}
