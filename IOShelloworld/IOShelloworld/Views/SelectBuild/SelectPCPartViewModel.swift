//
//  SelectPCPartViewModel.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 04/11/2021.
//

import Foundation

class SelectPCPartViewModel<T : PCPart>: ObservableObject {
    var apiService : APIService<T>
    
    @Published var selectedPartBrand : String = ""
    var parts : [T] = []
    var usableParts : [T] {
        if selectedPartBrand.isEmpty {
            return parts
        }
        
        return parts.filter{part in
            return part.brand == selectedPartBrand
    }}
    var partBrands : [String] {
        return Array(Set(parts.map{part in return part.brand}))
    }
    @Published var selectedPartId : Int = -1
    var selectedPart : T? {
        return parts.filter{part in
            return part.id == selectedPartId
            
        }.first as T?
    }
    @Published var hasLoaded = false
    
    init(apiService : APIService<T>){
        self.apiService = apiService
    }
    
    func getParts() {
        apiService.getAll(){(parts) -> () in
            self.parts = parts
            self.hasLoaded = true
        }
    }
}
