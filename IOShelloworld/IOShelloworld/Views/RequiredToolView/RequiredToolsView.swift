//
//  RequiredToolsView.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import SwiftUI

struct RequiredToolsView: View {
    @ObservedObject var requiredToolsViewModel : RequiredToolsViewModel
    
    @Inject("assemblyOverviewView") var assemblyOverviewView : AssemblyOverviewView
    
    init(_ requiredToolsViewModel : RequiredToolsViewModel){
        self.requiredToolsViewModel = requiredToolsViewModel
    }
    
    var body: some View {
        VStack(spacing: 20){
            List(requiredToolsViewModel.tools, id: \.id){ tool in
                    NavigationLink(destination: ToolDetailView(tool : tool)) {
                        Text(tool.name)
                    }
                    
                }
            HStack(spacing: 20) {
                NavigationLink(destination: assemblyOverviewView) {
                    Text("Start")
                }
                .navigationTitle("Required tools")
            }
        }.onAppear{requiredToolsViewModel.getTools()}
    }
}

