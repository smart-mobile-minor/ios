//
//  AssemblyStepView.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import SwiftUI
import RealityKit
import ARKit

struct AssemblyStepView: View {
    @Environment(\.presentationMode) var presentationMode: Binding<PresentationMode>
    var step : AssemblyStep
    var assemblyOverviewViewModel: AssemblyOverviewViewModel
    
    @State private var startPlacement = false
    @State private var modelConfirmedForPlacement: ARModel?
    @State private var modelsToBePlaced: [ARModel] = []
    @State private var anchorEntity = AnchorEntity(plane: .any)
    
    
    
    func loadModels(){
        self.startPlacement = true
        
        for name in step.armodels {
            ARModel(modelName: name){(arModel: ARModel) -> () in
                modelsToBePlaced.append(arModel)
            }
        }
                                  
        self.startPlacement = false
    }
    
    var body: some View {
        VStack(spacing: 20){
            placementButtonView(
                startPlacement: self.$startPlacement,
                modelConfirmedForPlacement: self.$modelConfirmedForPlacement,
                modelsToBePlaced: self.$modelsToBePlaced,
                anchorEntity: self.$anchorEntity,
                step: self.step
            )
            ARViewContainer(
                modelConfirmedForPlacement: self.$modelConfirmedForPlacement,
                modelsToBePlaced: self.$modelsToBePlaced,
                anchorEntity: self.$anchorEntity
            )
                    HStack(spacing: 10){
                        //ARViewContainer()
                        
                        VStack(spacing: 20){
                            Text(step.name)
                            Text(step.description)
                          
                                Button(action: {
                                    if (!step.completed){
                                    assemblyOverviewViewModel.completeStep()
                                    }
                                    self.presentationMode.wrappedValue.dismiss()
                                }) {
                                    Text("Finished")
                                }
                                .navigationTitle("Tool details")
                        }
                    }
        }.onAppear{loadModels()}
    }
    
    
}

struct placementButtonView: View{
    @Binding var startPlacement: Bool
    @Binding var modelConfirmedForPlacement: ARModel?
    @Binding var modelsToBePlaced: [ARModel]
    @Binding var anchorEntity: AnchorEntity
    var step : AssemblyStep
    var body: some View{
        Button("plaats ar model", action:{
            //let model = ARModel(modelName: step.armodels[1])
            debugPrint(modelsToBePlaced)
            var index : Int = 0
            if(anchorEntity.children.count == 1){
                index += 1
            }
            
            anchorEntity = AnchorEntity(plane: .any)
            self.modelConfirmedForPlacement = modelsToBePlaced[0]
            //self.startPlacement = false;
        }).disabled(modelsToBePlaced.count < step.armodels.count)
    }
}



struct ARViewContainer: UIViewRepresentable {
    @Binding var modelConfirmedForPlacement: ARModel?
    @Binding var modelsToBePlaced: [ARModel]
    @Binding var anchorEntity: AnchorEntity
    func makeUIView(context: Context) -> ARView {
        
        let arView = ARView(frame: .zero)
        
        let config = ARWorldTrackingConfiguration()
        config.planeDetection = [.horizontal, .vertical]
        config.environmentTexturing = .automatic
        
        if ARWorldTrackingConfiguration
            .supportsSceneReconstruction(.mesh){
            config.sceneReconstruction = .mesh
        }
        
        arView.session.run(config)
        
        return arView
        
    }
    
    func makeUIView(context: Context) -> some ARView {
        let arView = ARView(frame: .zero)
        
        let config = ARWorldTrackingConfiguration()
        config.planeDetection = [.horizontal, .vertical]
        config.environmentTexturing = .automatic
        
        if ARWorldTrackingConfiguration
            .supportsSceneReconstruction(.mesh){
            config.sceneReconstruction = .mesh
        }
        
        arView.session.run(config)
        
        return arView
    }
   
    func updateUIView(_ uiView: ARView, context: Context) {
        //TODO: load in all models from viewmodel included on button press
        if let model =
            self.modelConfirmedForPlacement{
            if let modelEntity = model.modelEntity{
                print("DEBUG: adding mnodel to scene - \(model.modelName)")
                anchorEntity.addChild(modelEntity)
                test()
                print("DEBUG: adding mnodel to scene - \(anchorEntity)")
                uiView.scene.addAnchor(anchorEntity)
                if(anchorEntity.children.count == 1){
                    print("WOOHOOOO")
                }
                
                //print("DEBUG: adding mnodel to scene - \(uiView.scene)")

            }
            else {
                print("DEBUG: Unable to load model to scene - \(model.modelName)")
            }

            DispatchQueue.main.async {
                self.modelConfirmedForPlacement = nil
            }
        }
    }
    
    func test(){
        let file = "test234"
        
        print("DEBUG: adding mnodel to scene - \(file)")
    }
}
