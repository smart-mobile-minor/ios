//
//  RequiredToolsViewModel.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 15/10/2021.
//

import Foundation

final class RequiredToolsViewModel : ObservableObject {
    @Inject("apiServiceTool") var toolsService : APIService<Tool>
    @Published var tools : [Tool] = []
    @Published var hasLoaded = false
    
    func getTools(){
        toolsService.getAll(){(response) -> () in
            self.tools = response
            self.hasLoaded = true
        }
    }
}
