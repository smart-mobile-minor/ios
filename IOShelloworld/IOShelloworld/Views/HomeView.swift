//
//  ContentView.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 28/09/2021.
//

import SwiftUI

struct ContentView: View {
    @Inject("selectProcessorView") var selectProcessor : AnyView
    @Inject("troubleshootingView") var troubleshootingView : TroubleshootingView
    
    var body: some View {
        NavigationView {
            VStack(spacing: 20){
                
                NavigationLink(destination: selectProcessor) {
                    Text("Start building!")
                }
                NavigationLink(destination: troubleshootingView) {
                    Text("Start troubleshooting")
                }
                
            }
            
        }
    }
    
    
}
