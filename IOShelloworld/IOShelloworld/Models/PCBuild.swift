//
//  PCBuild.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 04/11/2021.
//

import Foundation

struct PCBuild : Encodable {
    var processor: Processor
    var motherboard: Motherboard
    var pcCase : Case
    var powerSupply: PowerSupply
}
