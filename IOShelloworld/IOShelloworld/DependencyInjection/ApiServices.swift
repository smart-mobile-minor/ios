//
//  ApiServices.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation

class APIServices {
    static func addApiServicesToInjectables() {
        Injectables.createInjectable(reference: "apiServiceProcessor"){
            () -> APIService<Processor> in return APIService<Processor>(serviceSpecificUrl: "processor")}
        Injectables.createInjectable(reference: "apiServiceMotherboard"){
            () -> APIService<Motherboard> in return APIService<Motherboard>(serviceSpecificUrl: "motherboard")}
        Injectables.createInjectable(reference: "apiServiceCase"){
            () -> APIService<Case> in return APIService<Case>(serviceSpecificUrl: "case")}
        Injectables.createInjectable(reference: "apiServicePowerSupply"){
            () -> APIService<PowerSupply> in return APIService<PowerSupply>(serviceSpecificUrl: "powersupply")
        }
        
        Injectables.createInjectable(reference: "apiServiceTool"){
            () -> APIService<Tool> in return APIService<Tool>(serviceSpecificUrl: "tool")
        }
        Injectables.createInjectable(reference: "apiServiceAssemblyStep"){
            () -> APIService<AssemblyStep> in return APIService<AssemblyStep>(serviceSpecificUrl: "getbuild")
        }
    }
}
