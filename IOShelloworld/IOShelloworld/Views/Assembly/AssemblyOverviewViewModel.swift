//
//  AssemblyOverviewViewModel.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 15/10/2021.
//

import Foundation

final class AssemblyOverviewViewModel: ObservableObject {
    @Inject("apiServiceAssemblyStep") var asemblyStepService: APIService<AssemblyStep>
    @Inject("buildViewModel") var buildViewModel: PCBuildViewModel
    @Published var assemblySteps: [AssemblyStep] = []
    @Published var hasLoaded = false
    
    var currentStepNumber: Int {
        let currentNumber = assemblySteps.filter{step in return step.completed}.count + 1
        if(currentNumber > assemblyStepsLength){
            return assemblyStepsLength
        }
        return currentNumber
    }
    var assemblyStepsLength : Int { assemblySteps.count}
    
    func getAssemblySteps() {
        if (!assemblySteps.isEmpty){
            return
        }
        
        asemblyStepService.postAll(body: buildViewModel.getBuild()){ (response) -> () in
            self.assemblySteps = response
            //download models based upon string list
            //make armodel objects with downloaded images
            self.hasLoaded = true
        }
    }
    
    func completeStep() {
        var step = assemblySteps[currentStepNumber - 1]
        step.completed = true
        
        assemblySteps[currentStepNumber - 1] = step
    }
}
