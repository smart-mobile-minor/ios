//
//  ToolDetailView.swift
//  IOShelloworld
//
//  Created by Sidney Michiels on 30/09/2021.
//

import SwiftUI

struct ToolDetailView: View {
    var tool : Tool
    var body: some View {
        VStack(spacing: 20){
            Text(tool.name)
            Text(tool.image)
            Text(tool.description)
        }
        
    }
}
