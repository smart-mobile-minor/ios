//
//  Views.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation
import SwiftUI

class Views {
    static func addViewsToInjectables() {
        let buildViewModel: PCBuildViewModel = Injectables["buildViewModel"]
        
        Injectables.createInjectable(reference: "selectProcessorView"){
            () -> AnyView in
            return AnyView(SelectPCPartView<Processor>(
                selectPartViewModel: Injectables["selectProcessorViewModel"],
                buildViewModel: buildViewModel,
                nextPage: Injectables["selectMotherboardView"],
                partName: "processor"))
        }
        Injectables.createInjectable(reference: "selectMotherboardView"){
            () -> AnyView in
            return AnyView(SelectPCPartView<Motherboard>(
                selectPartViewModel: Injectables["selectMotherboardViewModel"],
                buildViewModel: buildViewModel,
                nextPage: Injectables["selectCaseView"],
                partName: "motherboard"))
        }
        Injectables.createInjectable(reference: "selectCaseView"){
            () -> AnyView in
            return AnyView(SelectPCPartView<Case>(
                selectPartViewModel: Injectables["selectCaseViewModel"],
                buildViewModel: buildViewModel,
                nextPage: Injectables["selectPowerSupplyView"],
                partName: "case"))
        }
        Injectables.createInjectable(reference: "selectPowerSupplyView"){
            () -> AnyView in
            return AnyView(SelectPCPartView<PowerSupply>(
                selectPartViewModel: Injectables["selectPowerSupplyViewModel"],
                buildViewModel: buildViewModel,
                nextPage: Injectables["requiredToolsView"],
                partName: "power supply"))
        }
        
        Injectables.createInjectable(reference: "requiredToolsView"){
            () -> AnyView in return AnyView(RequiredToolsView(Injectables["requiredToolsViewModel"]))
        }
        Injectables.createInjectable(reference: "troubleshootingView"){
            () -> TroubleshootingView in return TroubleshootingView()
        }
        Injectables.createInjectable(reference: "assemblyOverviewView"){
            () -> AssemblyOverviewView in return AssemblyOverviewView(assemblyOverviewViewModel: Injectables["assemblyOverviewViewModel"])
        }
    }
}
