//
//  PowerSupply.swift
//  IOShelloworld
//
//  Created by Sid Michiels on 15/10/2021.
//

import Foundation

struct PowerSupply : PCPart {
    var id: Int
    var name : String
    var brand : String
    var type : String
    var capacity : Int
    var image : String
}
