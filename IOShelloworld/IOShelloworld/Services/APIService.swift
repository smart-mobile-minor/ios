//
//  APIService.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 23/10/2021.
//

import Foundation

import Alamofire

class APIService<T : Codable> {
    private var baseUrl = "http://fe9e-145-93-172-69.ngrok.io/api/"
    var serviceSpecificUrl: String
    
    init(serviceSpecificUrl: String = ""){
        self.serviceSpecificUrl = serviceSpecificUrl
    }
    
    func getAll(completion: @escaping ([T]) -> Void) {
        AF.request(baseUrl + serviceSpecificUrl)
            .validate(statusCode: 200..<300)
            .responseData{ response in
                switch response.result {
                    case let .success(data):
                    let decoder = JSONDecoder()
                    do {
                        let parsedData = try decoder.decode([T].self, from: data)
                        completion(parsedData)
                    } catch {
                        print("Parse error")
                        print(error.localizedDescription)
                    }
                    case let .failure(error):
                        print("Request error")
                        print(error)
            }
        }
    }
        
        func postAll<X: Encodable>(body: X, completion: @escaping ([T]) -> Void) {
            AF.request(baseUrl + serviceSpecificUrl,
                       method: .post,
                       parameters: body,
                       encoder: JSONParameterEncoder.default)
                .validate(statusCode: 200..<300)
                .responseData{response in
                    switch response.result {
                        case let .success(data):
                        let decoder = JSONDecoder()
                        do {
                            let parsedData = try decoder.decode([T].self, from: data)
                            completion(parsedData)
                        } catch {
                            print("Parse error")
                            print(error.localizedDescription)
                        }
                        case let .failure(error):
                            print("Request error")
                            print(error)
                }
        }
    }
}
