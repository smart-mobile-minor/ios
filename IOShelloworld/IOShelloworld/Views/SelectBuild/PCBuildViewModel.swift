//
//  PCBuild.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 03/11/2021.
//

import Foundation

class PCBuildViewModel : ObservableObject {
    private var processor: Processor?
    private var motherboard: Motherboard?
    private var pcCase : Case?
    private var powerSupply: PowerSupply?
    
    func setPart(part: AnyObject){
        if part is Processor {
            processor = (part as? Processor)!
        }
        if part is Motherboard {
            motherboard = (part as? Motherboard)!
        }
        if part is Case {
            pcCase = (part as? Case)!
        }
        if part is PowerSupply {
            powerSupply = (part as? PowerSupply)!
        }
    }
    
    func getBuild() -> PCBuild {
        return PCBuild(processor: processor!, motherboard: motherboard!, pcCase: pcCase!, powerSupply: powerSupply!)
    }
}
