//
//  ViewModels.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation

class ViewModels {
    static func addViewModelsToInjectables() {
        let buildViewModel = PCBuildViewModel()
        Injectables.createInjectable(reference: "buildViewModel"){() -> PCBuildViewModel in return buildViewModel}
        
        Injectables.createInjectable(reference: "selectProcessorViewModel"){
            () -> SelectPCPartViewModel<Processor> in return SelectPCPartViewModel<Processor>(apiService: Injectables["apiServiceProcessor"])
        }
        Injectables.createInjectable(reference: "selectMotherboardViewModel"){
            () -> SelectPCPartViewModel<Motherboard> in return SelectPCPartViewModel<Motherboard>(apiService: Injectables["apiServiceMotherboard"])
        }
        Injectables.createInjectable(reference: "selectCaseViewModel"){
            () -> SelectPCPartViewModel<Case> in return SelectPCPartViewModel<Case>(apiService: Injectables["apiServiceCase"])
        }
        Injectables.createInjectable(reference: "selectPowerSupplyViewModel"){
            () -> SelectPCPartViewModel<PowerSupply> in return SelectPCPartViewModel<PowerSupply>(apiService: Injectables["apiServicePowerSupply"])
        }
        
        Injectables.createInjectable(reference: "requiredToolsViewModel"){
            () -> RequiredToolsViewModel in return RequiredToolsViewModel()
        }
        Injectables.createInjectable(reference: "assemblyOverviewViewModel"){
            () -> AssemblyOverviewViewModel in return AssemblyOverviewViewModel()
        }
    }
    
}
