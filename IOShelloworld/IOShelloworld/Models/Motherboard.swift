//
//  Motherboard.swift
//  IOShelloworld
//
//  Created by Sid Michiels on 15/10/2021.
//

import Foundation

struct Motherboard : PCPart {
    var id: Int
    var name : String
    var brand : String
    var socketType : String
    var image : String
}
