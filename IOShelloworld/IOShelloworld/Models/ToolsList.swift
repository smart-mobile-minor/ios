//
//  ToolsList.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 15/10/2021.
//

import Foundation

struct ToolsList: Decodable {
    let status: String
    let tools: [Tool]
}
