//
//  Injectables.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation

struct Injectables {
    private static var injectables : [Injectable] = []
    
    private static func getInjectableByReference(injectableReference: String) -> Injectable{
        print(injectableReference)
        return injectables.filter{injectable in return injectable.reference == injectableReference}.first!
    }
    
    static subscript<T>(injectableReference: String) -> T {
        return getInjectableByReference(injectableReference: injectableReference).value as! T
    }
    
    static func createInjectable(reference: String, createMethod: @escaping () -> Any){
        injectables.append(Injectable(reference: reference, createInjectable: createMethod))
    }
    
    static func removeInjectable(reference: String){
        injectables = injectables.filter{injectable in return injectable.reference != reference}
    }
    
    static func replaceInjectable(reference: String, createMethod: @escaping () -> Any){
        removeInjectable(reference: reference)
        createInjectable(reference: reference, createMethod: createMethod)
    }
}
