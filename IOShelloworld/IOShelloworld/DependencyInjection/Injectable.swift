//
//  Injectable.swift
//  IOShelloworld
//
//  Created by Niek Sleddens on 05/11/2021.
//

import Foundation

struct Injectable {
    var reference : String
    var value : Any { return createInjectable() }
    var createInjectable : () -> Any
}
